//
// Created by vinra on 24/11/2021.
//

#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <stdlib.h>

int block_first(int id, int p, int n);
int block_size(int id, int p, int n);

int main(int argc, char* argv[]) {
    int i; //indice local
    int id; //id de la machine
    int p;  //nombre de processus
    int j;  //indice global
    int begin_val; //valeur à partir de laquelle on vérifie si les nombres sont premiers dans le tableau
    int global_count; //nombre de couples
    double time;
    int* tab; //tableaux associés aux machines
    MPI_Init(&argc, &argv);
    MPI_Barrier(MPI_COMM_WORLD);
    time = -MPI_Wtime();

    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    int n = atoi(argv[1]);  //nombre d'éléments
    int first_value=2+block_first(id, p, n);  //première valeur du morceau de tableau de associé à la machine id
    int last_value=2+block_first(id+1, p, n)-1; //dernière " " "
    int size=block_size(id, p, n); //taille de ce tableau
    tab= malloc(size * sizeof (int));
    if (tab==NULL){
        printf("Error, cannot allocate memory for process %d!\n", id);
        MPI_Finalize(); exit(1);
    }
    for (i = 0; i < size; i++) {  //on les considère premier dès le départ
        tab[i] = 1;
    }
    if (!id){  //si on est la machine 0 ("maître d'oeuvre") on dit que 0 est non premier et on initialise l'indice global
        tab[0]=0;
        j=0;
    }
    int k=2;  //le premier nombre premier à partir duquel on va effectuer les calculs
    do {
        if (k*k>first_value){  //Pour savoir d'où on part dans le tableau
            begin_val=k*k-first_value;
        }
        else{
            if (!(first_value%k)) k=0;
            else begin_val=k-(first_value%k);
        }
        for (i = begin_val; i < size; i += k) { //Une fois qu'on sait d'où on part on parcoure le tableau pour trouver les multiples de k
            tab[i] = 0;
        }
        if (!id){ //la machine 0 cherche le nouveau k
            while(!tab[++j]);
            k = j+2;
        }
        MPI_Bcast(&k, 1, MPI_INT, 0, MPI_COMM_WORLD); //On donne la nouvelle valeur de k à tout le monde
    }while(k*k <= n);
    int count=0; //sous-total de couples de nombres premiers
    for(i = 0; i<size; i++){ //Affectation du sous-total
        if(tab[i] && tab[i+2]){
            count++;
        }
    }
    MPI_Reduce(&count, &global_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); //réduction avec toutes les machines
    time+=MPI_Wtime(); //conclusion
    if (!id){
        printf("Il y a %d couples de nombres entiers premiers jumeaux dans %d.\n", global_count, n);
        printf("Algorithme exécuté en %f secondes", time);
    }
    fflush(stdout); //libération de la mémoire
    MPI_Finalize();
    return 0;
}

int block_first(int id, int p, int n){
    return ((id*n)/p);
}

int block_size(int id, int p, int n){
    return ((block_first(id+1, p, n)-1)- block_first(id, p, n));
}
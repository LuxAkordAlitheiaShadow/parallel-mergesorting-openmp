//
// Created by Titouan 'Lux' Allain on 11/21/21.
//

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>

#define THRESHOLD_INSERT_SORTING 1000
#define MAX_ACTIVE_THREAD 10000

struct TZone {
    int min;
    int max;
} t_zone;

int* sorted_tab;

void insert_sorting(struct TZone mother_zone)
{
    // Declaring variables
    int t_i_index;
    int t_j_index;
    int key;
    // Sorting tab in loop
    for ( t_j_index = mother_zone.min + 1; t_j_index < mother_zone.max ; t_j_index++ )
    {
        key = sorted_tab[t_j_index];
        t_i_index = t_j_index - 1;
        // Shifting value until is greater than key value
        while ( t_i_index >= mother_zone.min && sorted_tab[t_i_index] > key)
        {
            sorted_tab[t_i_index + 1] = sorted_tab[t_i_index];
            t_i_index--;
        }
        sorted_tab[t_i_index + 1] = key;
    }
}

void merge(struct TZone u_zone, struct TZone v_zone, struct TZone mother_zone)
{
    // Creating U and V tabs
    int* u_tab = malloc((u_zone.max - u_zone.min + 1) * sizeof(int));
    int* v_tab = malloc((v_zone.max - v_zone.min + 1) * sizeof(int));
    int sorted_index;

    // Copying data in U tab
    int u_index = 0;
    for (sorted_index = u_zone.min ; sorted_index < u_zone.max ; sorted_index++)
    {
        u_tab[u_index] = sorted_tab[sorted_index];
        u_index++;
    }

    // Copying data in V tab
    int v_index = 0;
    for (sorted_index = v_zone.min ; sorted_index < v_zone.max ; sorted_index++)
    {
        v_tab[v_index] = sorted_tab[sorted_index];
        v_index++;
    }

    // Adding max value possible for last element of half tabs
    u_tab[u_zone.max - u_zone.min] = INT_MAX;
    v_tab[v_zone.max - v_zone.min] = INT_MAX;

    u_index = 0;
    v_index = 0;
    int k_index;
    // Affecting sorted values in tab to sort in loop
    for ( k_index = mother_zone.min ; k_index < mother_zone.max; k_index++ )
    {
        // First half tab element is lower than second half tab element statement
        if ( u_tab[u_index] < v_tab[v_index])
        {
            sorted_tab[k_index] = u_tab[u_index++];
        }
        // First half tab element is greater than second half tab element statement
        else
        {
            sorted_tab[k_index] = v_tab[v_index++];
        }
    }

    // Releasing U and V tabs
    free(u_tab);
    free(v_tab);
}

void merge_sorting(struct TZone mother_zone)
{
    // Insert sorting statement
    if ( mother_zone.max - mother_zone.min <= THRESHOLD_INSERT_SORTING )
    {
        insert_sorting(mother_zone);
    }
    // Merge sorting statement
    else
    {
        // Declaring half-zone
        struct TZone u_zone;
        struct TZone v_zone;

        // Setting first half zone
        u_zone.min = mother_zone.min;
        // Tab to sort is fairly separable statement
        if ( (mother_zone.max - mother_zone.min) % 2 == 0 )
        {
            u_zone.max = (mother_zone.max - mother_zone.min) / 2 + mother_zone.min;
        }
        // Tab to sort isn't fairly separable statement
        else
        {
            u_zone.max = (mother_zone.max - mother_zone.min) / 2 + 1 + mother_zone.min;
        }

        // Setting second half zone
        v_zone.min = u_zone.max;
        v_zone.max = mother_zone.max;

        // Sorting the half tabs
        #pragma omp parallel sections
        {
            merge_sorting(u_zone);
            #pragma omp section
            {
                merge_sorting(v_zone);
            }
        }

        // Merging half-tabs
        merge(u_zone, v_zone, mother_zone);
    }
}

int main()
{
    printf("Start of the program\n");
    printf("Parallel sorting with OpenMP selected\n");

    // Setting max running thread
    omp_set_num_threads(MAX_ACTIVE_THREAD);

    // Declaring mother tab and zone
    struct TZone mother_zone;

    // Tab size selection
    int tab_size;
    printf("Please enter the size of the array : ");
    scanf("%d",&tab_size);

    mother_zone.min = 0;
    mother_zone.max = tab_size;

    // Allocating tab to sort and sorted tab
    sorted_tab = malloc(tab_size * sizeof(int));

    // Generating values for tab
    int t_index;
    printf("Affecting values to tab...\n");
    #pragma omp parallel for
    for( t_index = 0 ; t_index < tab_size ; t_index++)
    {
        sorted_tab[t_index] = rand();
    }

    // Initializing clock
    struct timespec current_time;
    long start_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    start_time=current_time.tv_sec;

    printf("Sorting tab...\n");

    // Insert sorting statement
    if (tab_size < THRESHOLD_INSERT_SORTING)
    {
        insert_sorting(mother_zone);
    }
    // Merge sorting statement
    else
    {
        // Parallel sorting
        merge_sorting(mother_zone);
    }

    // Getting end time of program
    long end_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    end_time=current_time.tv_sec;

    // Calculating execution time
    long execution_time;
    execution_time = end_time - start_time;
    printf("Total execution time : %ld seconds.\n\n", execution_time);

    // Printing 10 first sorted values
    printf("10 first sorted values :");
    for( t_index = 0; t_index < 10 ; t_index++)
    {
        printf("%d ; ",sorted_tab[t_index]);
    }
    printf("\n");

    // Printing 10 last sorted values
    printf("10 last sorted values :");
    for( t_index = tab_size - 10; t_index < tab_size ; t_index++)
    {
        printf("%d ; ",sorted_tab[t_index]);
    }
    printf("\n");

    free(sorted_tab);
    printf("Exiting the program.\n");
    return 0;
}
